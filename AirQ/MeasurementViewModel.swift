//
//  MeasurementViewModel.swift
//  AirQ
//
//  Created by Marcin Rainka on 19/06/2017.
//  Copyright © 2017 Atos. All rights reserved.
//

import UIKit

final class MeasurementViewModel {

    let parameter: String
    let unit: String
    let value: String

    init(_ measurement: Measurement) {
        parameter = measurement.parameter?.uppercased() ?? ""
        unit = measurement.unit ?? ""
        value = type(of: self).value(from: measurement)
    }

    /** Rounds value to one decimal place only if value is not integer. */
    private static func value(from measurement: Measurement) -> String {
        guard let value = measurement.value else { return "" }
        return String(format: "%.\(value.isInteger ? 0 : 1)f", value)
    }
}

extension MeasurementViewModel: CollectionViewCompatible {

    func cell(for collectionView: UICollectionView, at indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: MeasurementCollectionViewCell.identifier,
            for: indexPath) as! MeasurementCollectionViewCell
        cell.configure(with: self)
        return cell
    }
}
