//
//  NavigationController.swift
//  AirQ
//
//  Created by Marcin Rainka on 19/06/2017.
//  Copyright © 2017 Atos. All rights reserved.
//

import UIKit

final class NavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNavigationBar()
    }

    private func setUpNavigationBar() {
        navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationBar.shadowImage = UIImage(color: .white, size: CGSize(width: 1, height: .hairline))
    }
}
