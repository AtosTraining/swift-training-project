//
//  URLError.swift
//  AirQ
//
//  Created by Marcin Rainka on 20/06/2017.
//  Copyright © 2017 Atos. All rights reserved.
//

enum URLError: Error {

    case percentEncodingUnaddable
}
