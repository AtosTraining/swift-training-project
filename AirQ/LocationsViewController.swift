//
//  LocationsViewController.swift
//  AirQ
//
//  Created by Marcin Rainka on 14/06/2017.
//  Copyright © 2017 Atos. All rights reserved.
//

import UIKit

final class LocationsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    var model: LocationsViewModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        configure(with: LocationsViewModel())
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        model?.fetchDataIfNeeded()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let model = model,
           let latestView = segue.destination as? LatestViewController,
           let location = (sender as? LocationTableViewCell)?.model {
            model.configure(latestView, with: location)
        }
    }
}

extension LocationsViewController: Configurable {

    func configure(with model: LocationsViewModel) {
        self.model = model

        model.locationsCallback = { _ in self.tableView.reloadData() }

        model.isFetchingDataCallback = {
            $0 ? self.activityIndicator.startAnimating() : self.activityIndicator.stopAnimating()
        }

        model.onFetchingError = { self.showAlert(withTitle: $0) }
    }
}

extension LocationsViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return model?.locations[indexPath.row].cell(for: tableView, at: indexPath) ?? UITableViewCell()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model?.locations.count ?? 0
    }
}
