//
//  LocationTableViewCell.swift
//  AirQ
//
//  Created by Marcin Rainka on 14/06/2017.
//  Copyright © 2017 Atos. All rights reserved.
//

import UIKit

class LocationTableViewCell: UITableViewCell {

    static let identifier = "LocationTableViewCell"

    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!

    var model: LocationViewModel?

    override func awakeFromNib() {
        super.awakeFromNib()
        setUpSelectedBackgroundView()
    }

    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
        recolor()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        recolor()
    }

    private func recolor() {
        tintColor = isHighlighted || isSelected ? .black : .white
    }

    private func setUpSelectedBackgroundView() {
        let view = UIView()
        view.backgroundColor = .white
        selectedBackgroundView = view
    }
}

extension LocationTableViewCell: Configurable {

    func configure(with model: LocationViewModel) {
        self.model = model

        cityLabel.text = model.city
        locationLabel.text = model.location
    }
}
