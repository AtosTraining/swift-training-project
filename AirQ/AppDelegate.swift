//
//  AppDelegate.swift
//  AirQ
//
//  Created by Marcin Rainka on 13/06/2017.
//  Copyright © 2017 Atos. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(
            _ application: UIApplication,
            didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        sleep(2)  // Just to admire Atos logo on splash screen for longer.
        return true
    }
}
