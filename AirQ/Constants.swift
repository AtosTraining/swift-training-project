//
//  Constants.swift
//  AirQ
//
//  Created by Marcin Rainka on 14/06/2017.
//  Copyright © 2017 Atos. All rights reserved.
//

import Foundation.NSURL
import UIKit

extension URL {

    static let base = "https://api.openaq.org/v1"

    static let locations = base + "/locations"
    static let latest = base + "/latest"
}

extension CGFloat {

    static let hairline = 1 / UIScreen.main.scale
}

extension UIColor {

    static let skyBlue = UIColor(red: 0, green: 178 / 255, blue: 238 / 255, alpha: 1)
}

extension UIFont {

    static let systemFontName = UIFont.systemFont(ofSize: CGFloat()).fontName  // Size doesn't matter...
}
