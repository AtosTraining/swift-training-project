//
//  PolishLocationsSpecification.swift
//  AirQ
//
//  Created by Marcin Rainka on 14/06/2017.
//  Copyright © 2017 Atos. All rights reserved.
//

import Foundation.NSURL

struct PolishLocationsSpecification: Specification {

    let url = URL.locations + "?country=PL"
    let keyPath = Optional("results")
}
