//
//  LocationsViewModel.swift
//  AirQ
//
//  Created by Marcin Rainka on 14/06/2017.
//  Copyright © 2017 Atos. All rights reserved.
//

final class LocationsViewModel {

    private(set) var locations = [LocationViewModel]() { didSet { locationsCallback?(locations) } }
    var locationsCallback: (([LocationViewModel]) -> Void)?

    private(set) var isFetchingData = false { didSet { isFetchingDataCallback?(isFetchingData) } }
    var isFetchingDataCallback: ((Bool) -> Void)?

    var isFetchingDataNeeded: Bool { return !isFetchingData && locations.isEmpty }

    var onFetchingError: ((String) -> Void)?

    func fetchDataIfNeeded() {
        guard isFetchingDataNeeded else { return }
        fetchData()
    }

    func fetchData() {
        isFetchingData = true

        Repository().query(with: PolishLocationsSpecification()) { [weak self] (locations: [Location], error) in
            guard let `self` = self else { return }
            self.locations = locations.map(LocationViewModel.init).sorted { $0.city < $1.city }
            self.isFetchingData = false
            if error != nil { self.onFetchingError?("Something went wrong".localized()) }
        }
    }

    func configure(_ latestView: LatestViewController, with location: LocationViewModel) {
        latestView.configure(with: LatestViewModel(location: location.model))
    }
}
