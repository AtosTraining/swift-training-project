//
//  LatestViewController.swift
//  AirQ
//
//  Created by Marcin Rainka on 14/06/2017.
//  Copyright © 2017 Atos. All rights reserved.
//

import UIKit

final class LatestViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    var model: LatestViewModel?

    fileprivate static let sectionInset = CGFloat(16)

    fileprivate static var numberOfItemsInRow: Int {
        return UIScreen.main.bounds.width > UIScreen.main.bounds.height ? 3 : 2
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpCollectionView()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        model?.fetchDataIfNeeded()
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)

        coordinator.animate(
            alongsideTransition: { _ in self.collectionView.collectionViewLayout.invalidateLayout() },
            completion: nil)
    }

    private func setUpCollectionView() {
        collectionView.dataSource = self
        collectionView.delegate = self
    }
}

extension LatestViewController: Configurable {

    func configure(with model: LatestViewModel) {
        self.model = model

        title = model.title

        model.measurementsCallback = { _ in self.collectionView.reloadData() }

        model.isFetchingDataCallback = {
            $0 ? self.activityIndicator.startAnimating() : self.activityIndicator.stopAnimating()
        }

        model.onFetchingError = { self.showAlert(withTitle: $0) }
    }
}

extension LatestViewController: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return model?.measurements.count ?? 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return model?.measurements[indexPath.row].cell(for: collectionView, at: indexPath) ?? UICollectionViewCell()
    }
}

extension LatestViewController: UICollectionViewDelegateFlowLayout {

    func collectionView(
            _ collectionView: UICollectionView,
            layout collectionViewLayout: UICollectionViewLayout,
            sizeForItemAt indexPath: IndexPath) -> CGSize {
        let availableWidth
            = view.bounds.width - type(of: self).sectionInset * (CGFloat(type(of: self).numberOfItemsInRow) + 1)

        let size = floor(availableWidth / CGFloat(type(of: self).numberOfItemsInRow))

        return CGSize(width: size, height: size)
    }

    func collectionView(
            _ collectionView: UICollectionView,
            layout collectionViewLayout: UICollectionViewLayout,
            insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(
            top: type(of: self).sectionInset,
            left: type(of: self).sectionInset,
            bottom: type(of: self).sectionInset,
            right: type(of: self).sectionInset)
    }

    func collectionView(
            _ collectionView: UICollectionView,
            layout collectionViewLayout: UICollectionViewLayout,
            minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return type(of: self).sectionInset
    }
}
