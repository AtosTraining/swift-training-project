//
//  Latest.swift
//  AirQ
//
//  Created by Marcin Rainka on 13/06/2017.
//  Copyright © 2017 Atos. All rights reserved.
//

import ObjectMapper

struct Latest {

    var measurements: [Measurement]?

    init?(map: Map) {}
}

extension Latest: Mappable {

    mutating func mapping(map: Map) {
        measurements <- map["measurements"]
    }
}
