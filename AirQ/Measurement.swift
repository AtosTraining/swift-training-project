//
//  Measurement.swift
//  AirQ
//
//  Created by Marcin Rainka on 13/06/2017.
//  Copyright © 2017 Atos. All rights reserved.
//

import ObjectMapper

struct Measurement {

    var parameter: String?
    var unit: String?
    var value: Float?

    init?(map: Map) {}
}

extension Measurement: Mappable {

    mutating func mapping(map: Map) {
        parameter <- map["parameter"]
        unit <- map["unit"]
        value <- map["value"]
    }
}
