//
//  Specification.swift
//  AirQ
//
//  Created by Marcin Rainka on 14/06/2017.
//  Copyright © 2017 Atos. All rights reserved.
//

protocol Specification {

    var url: String { get }
    var keyPath: String? { get }
}

extension Specification {

    var keyPath: String? { return nil }
}
