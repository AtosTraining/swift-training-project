//
//  LatestViewModel.swift
//  AirQ
//
//  Created by Marcin Rainka on 19/06/2017.
//  Copyright © 2017 Atos. All rights reserved.
//

final class LatestViewModel {

    let title: String

    private(set) var measurements = [MeasurementViewModel]() { didSet { measurementsCallback?(measurements) } }
    var measurementsCallback: (([MeasurementViewModel]) -> Void)?

    private(set) var isFetchingData = false { didSet { isFetchingDataCallback?(isFetchingData) } }
    var isFetchingDataCallback: ((Bool) -> Void)?

    var isFetchingDataNeeded: Bool { return !isFetchingData && measurements.isEmpty }

    var onFetchingError: ((String) -> Void)?

    private let location: Location

    init(location: Location) {
        self.location = location
        title = location.title
    }

    func fetchDataIfNeeded() {
        guard isFetchingDataNeeded else { return }
        fetchData()
    }

    func fetchData() {
        isFetchingData = true

        Repository().query(with: LatestSpecification(location: location)) { [weak self] (latests: [Latest], error) in
            guard let `self` = self else { return }
            self.measurements = latests.first?.measurements?.map(MeasurementViewModel.init) ?? []
            self.isFetchingData = false
            if error != nil { self.onFetchingError?("Something went wrong".localized()) }
        }
    }
}

fileprivate extension Location {

    var title: String {
        var title = ""

        if let city = city {
            title += city
        }

        if let location = location {
            title += title.isEmpty ? location : " (\(location))"
        }

        return title
    }
}
