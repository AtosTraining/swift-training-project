//
//  Utilities.swift
//  AirQ
//
//  Created by Marcin Rainka on 19/06/2017.
//  Copyright © 2017 Atos. All rights reserved.
//

import JSSAlertView
import UIKit

extension Float {

    var isInteger: Bool { return floorf(self) == self }
}

extension String {

    func localized(_ comment: String = "") -> String {
        return NSLocalizedString(self, comment: comment)
    }
}

// MARK: - UI

extension UIViewController {

    func showAlert(withTitle title: String, message: String? = nil) {
        let alertView = JSSAlertView().show(self, title: title, text: message, color: .skyBlue)
        alertView.setButtonFont(UIFont.systemFontName)
        alertView.setTextTheme(.light)
    }
}

extension UIImage {

    convenience init?(color: UIColor, size: CGSize = CGSize(width: 1, height: 1)) {
        let rect = CGRect(origin: .zero, size: size)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        guard let cgImage = image?.cgImage else { return nil }
        self.init(cgImage: cgImage)
    }
}
