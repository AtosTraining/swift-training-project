//
//  Location.swift
//  AirQ
//
//  Created by Marcin Rainka on 13/06/2017.
//  Copyright © 2017 Atos. All rights reserved.
//

import ObjectMapper

struct Location {

    var location: String?
    var city: String?

    init?(map: Map) {}
}

extension Location: Mappable {

    mutating func mapping(map: Map) {
        location <- map["location"]
        city <- map["city"]
    }
}
