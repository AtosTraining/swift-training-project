//
//  MeasurementCollectionViewCell.swift
//  AirQ
//
//  Created by Marcin Rainka on 14/06/2017.
//  Copyright © 2017 Atos. All rights reserved.
//

import UIKit

final class MeasurementCollectionViewCell: UICollectionViewCell {

    static let identifier = "MeasurementCollectionViewCell"

    @IBOutlet weak var parameterLabel: UILabel!
    @IBOutlet weak var unitLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!

    var model: MeasurementViewModel?

    override func awakeFromNib() {
        super.awakeFromNib()
        setUpBorder()
    }

    private func setUpBorder() {
        layer.borderWidth = .hairline
        layer.borderColor = UIColor.white.cgColor
        layer.cornerRadius = 6
    }
}

extension MeasurementCollectionViewCell: Configurable {

    func configure(with model: MeasurementViewModel) {
        self.model = model

        parameterLabel.text = model.parameter
        unitLabel.text = model.unit
        valueLabel.text = model.value
    }
}
