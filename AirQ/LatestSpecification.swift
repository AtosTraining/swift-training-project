//
//  LatestSpecification.swift
//  AirQ
//
//  Created by Marcin Rainka on 14/06/2017.
//  Copyright © 2017 Atos. All rights reserved.
//

import Foundation.NSURL

struct LatestSpecification: Specification {

    let url: String
    let keyPath = Optional("results")

    init(location: Location) {
        url = location.latestURL()
    }
}

fileprivate extension Location {

    func latestURL() -> String {
        var latestURL = URL.latest + "?";

        if let location = location {
            latestURL += "&location=" + location
        }

        if let city = city {
            latestURL += "&city=" + city
        }

        return latestURL;
    }
}
