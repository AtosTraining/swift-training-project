//
//  Repository.swift
//  AirQ
//
//  Created by Marcin Rainka on 13/06/2017.
//  Copyright © 2017 Atos. All rights reserved.
//

import Alamofire
import AlamofireObjectMapper
import ObjectMapper

final class Repository<T: Mappable> {

    func query(with specification: Specification, completion: @escaping ([T], Error?) -> Void) {
        guard let url = specification.url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else {
            completion([], URLError.percentEncodingUnaddable)
            return
        }

        Alamofire.request(url).validate().responseArray(keyPath: specification.keyPath) {
            completion($0.value ?? [], $0.error)
        }
    }
}
