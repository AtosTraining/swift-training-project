//
//  LocationViewModel.swift
//  AirQ
//
//  Created by Marcin Rainka on 14/06/2017.
//  Copyright © 2017 Atos. All rights reserved.
//

import UIKit

final class LocationViewModel {

    let model: Location

    let city: String
    let location: String

    init(_ location: Location) {
        model = location

        city = location.city ?? ""
        self.location = location.location ?? ""
    }
}

extension LocationViewModel: TableViewCompatible {

    func cell(for tableView: UITableView, at indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: LocationTableViewCell.identifier, for: indexPath)
            as! LocationTableViewCell
        cell.configure(with: self)
        return cell
    }
}
