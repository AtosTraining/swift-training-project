//
//  Protocols.swift
//  AirQ
//
//  Created by Marcin Rainka on 14/06/2017.
//  Copyright © 2017 Atos. All rights reserved.
//

import UIKit

protocol Configurable {

    associatedtype T

    var model: T? { get set }

    func configure(with model: T)
}

protocol TableViewCompatible {

    func cell(for tableView: UITableView, at indexPath: IndexPath) -> UITableViewCell
}

protocol CollectionViewCompatible {

    func cell(for collectionView: UICollectionView, at indexPath: IndexPath) -> UICollectionViewCell
}
